#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
test identifiable module
"""

import linkahead as db
import pytest

from caoscrawler.identifiable import Identifiable
from caoscrawler.sync_node import SyncNode


def test_create_hashable_string():
    assert Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B")) == "P<B>N<A>R<[]>"
    assert Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", properties={'a': 5})) == "P<B>N<A>R<[]>a:5"
    a = Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", properties={'a': 4, 'b': 5}))
    b = Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", properties={'b': 5, 'a': 4}))
    assert a == b
    assert (
        Identifiable._create_hashable_string(
            Identifiable(name="A", record_type="B",
                         properties={'a': SyncNode(db.Record(id=12))})
        ) == "P<B>N<A>R<[]>a:12")
    a = Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", properties={'a': [SyncNode(db.Record(id=12))]}))
    assert (a == "P<B>N<A>R<[]>a:[12]")
    assert (Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", properties={'a': [12]})) == "P<B>N<A>R<[]>a:[12]")
    assert (
        Identifiable._create_hashable_string(
            Identifiable(name="A", record_type="B", properties={
                         'a': [SyncNode(db.Record(id=12)), 11]})
        ) == "P<B>N<A>R<[]>a:[12, 11]")
    assert Identifiable._create_hashable_string(
        Identifiable(name="A", record_type="B", backrefs=[123, SyncNode(db.Record(id=124))],
                     properties={'a': 5})) == "P<B>N<A>R<['123', '124']>a:5"


def test_name():
    with pytest.raises(ValueError):
        Identifiable(properties={"Name": 'li'})


def test_repr():
    # only test that something meaningful is returned
    assert 'properties' in str(Identifiable(name="A", record_type="B"))
    assert str(Identifiable(name="A", record_type="B", properties={'a': 0})).split(
        "properties:\n")[1].split('\n')[0] == '{"a": "0"}'
    assert str(Identifiable(name="A", record_type="B", properties={'a': 0, 'b': "test"})).split(
        "properties:\n")[1].split('\n')[0] == '{"a": "0", "b": "test"}'

    # TODO(henrik): Add a test using backrefs once that's implemented.


def test_equality():
    assert Identifiable(
        record_id=12, properties={"a": 0}) == Identifiable(record_id=12, properties={"a": 1})
    assert Identifiable(
        record_id=12, properties={"a": 0}) != Identifiable(record_id=13, properties={"a": 0})
    assert Identifiable(
        record_id=12, properties={"a": 0}) == Identifiable(properties={"a": 0})
    assert Identifiable(properties={"a": 0}) == Identifiable(properties={"a": 0})
    assert Identifiable(properties={"a": 0}) != Identifiable(properties={"a": 1})
