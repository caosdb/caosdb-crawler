#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Alexander Schlemmer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

from functools import partial
from os.path import basename, dirname, join
from pathlib import Path
from unittest.mock import MagicMock, Mock

import linkahead as db
import pytest
import yaml
from linkahead.apiutils import compare_entities
from pytest import raises
from utils import dircheckstr as dircheckstr_base

from caoscrawler import Crawler
from caoscrawler.debug_tree import DebugTree
from caoscrawler.identifiable_adapters import (IdentifiableAdapter,
                                               LocalStorageIdentifiableAdapter)
from caoscrawler.scanner import scan_directory
from caoscrawler.structure_elements import (DictListElement, DictTextElement,
                                            File)

UNITTESTDIR = Path(__file__).parent
dircheckstr = partial(dircheckstr_base, UNITTESTDIR / "test_directories" /
                      "example_substitutions")


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return join(dirname(__file__), *pathcomponents)


def test_substitutions():

    dbt = DebugTree()
    scan_directory(rfp("test_directories", "example_substitutions", "ExperimentalData"),
                   rfp("test_directories", "example_substitutions", "substitutions.yml"),
                   debug_tree=dbt)
    # @review Florian Spreckelsen 2022-05-13
    for i in range(2):
        subd = dbt.debug_tree[dircheckstr("ExperimentalData", "220512_data.dat")]
        assert subd[i]["Experiment"].get_property("date").value == "2022-05-12"
        assert isinstance(subd[i]["ExperimentSeries"].get_property(
            "Experiment").value, db.Record)

        subd = dbt.debug_tree[dircheckstr("ExperimentalData")]
        assert subd[i]["Project"].name == "project"
        assert isinstance(subd[i]["Project"].get_property(
            "Experiments").value, list)
        assert isinstance(subd[i]["Project"].get_property(
            "Experiments").value[0], db.Record)

        assert isinstance(subd[i]["Project"].get_property("dates").value, list)
        assert subd[i]["Project"].get_property(
            "dates").value[0] == "2022-05-12"


def test_substitutions_parents():
    dbt = DebugTree()
    scan_directory(rfp("test_directories", "example_substitutions", "ExperimentalData"),
                   rfp("test_directories", "example_substitutions",
                       "substitutions_parents.yml"),
                   debug_tree=dbt)
    # This is a test for:
    # https://gitlab.indiscale.com/caosdb/src/caosdb-crawler/-/issues/35
    # ... testing whether variable substitutions can be used in parent declarations.
    subd = dbt.debug_tree[dircheckstr("ExperimentalData", "220512_data.dat")]
    # subd[0] <- generalStore
    # subd[1] <- recordStore

    parents = subd[1]["Experiment"].get_parents()
    assert len(parents) == 2
    assert parents[0].name == "Experiment"
    assert parents[1].name == "Month_05"


def test_empty_parents():
    dbt = DebugTree()
    scan_directory(rfp("test_directories", "example_substitutions", "ExperimentalData"),
                   rfp("test_directories", "example_substitutions",
                       "substitutions_parents.yml"),
                   debug_tree=dbt)
    # This is a test for:
    # https://gitlab.com/caosdb/caosdb-crawler/-/issues/8

    subd = dbt.debug_tree[dircheckstr("ExperimentalData", "220512_data.dat")]

    parents = subd[1]["RecordWithoutParents"].get_parents()
    assert len(parents) == 0

    parents = subd[1]["RecordThatGetsParentsLater"].get_parents()
    assert len(parents) == 1
    assert parents[0].name == "Month_05"
