# Installation ##


## Linux ####

Make sure that Python (at least version 3.8) and pip is installed, using your system tools and
documentation.

Then open a terminal and continue in the [Generic installation](#generic-installation) section.

## Windows ####

If a Python distribution is not yet installed, we recommend Anaconda Python, which you can download
for free from [https://www.anaconda.com](https://www.anaconda.com).  The "Anaconda Individual Edition" provides most of all
packages you will ever need out of the box.  If you prefer, you may also install the leaner
"Miniconda" installer, which allows you to install packages as you need them.

After installation, open an Anaconda prompt from the Windows menu and continue in the [Generic
installation](#generic-installation) section.

## MacOS ####

If there is no Python 3 installed yet, there are two main ways to
obtain it: Either get the binary package from
[python.org](https://www.python.org/downloads/) or, for advanced
users, install via [Homebrew](https://brew.sh/). After installation
from python.org, it is recommended to also update the TLS certificates
for Python (this requires administrator rights for your user):

```sh
# Replace this with your Python version number:
cd /Applications/Python\ 3.9/

# This needs administrator rights:
sudo ./Install\ Certificates.command
```

After these steps, you may continue with the [Generic
installation](#generic-installation).

## Generic installation ####

The CaosDB crawler is available as [PyPi
package](https://pypi.org/project/caoscrawler/) and can simply installed by

```sh
pip3 install caoscrawler
```

Alternatively, obtain the sources from GitLab and install from there (`git` must
be installed for this option):

```sh
git clone https://gitlab.com/caosdb/caosdb-crawler
cd caosdb-crawler
pip3 install --user .
```
