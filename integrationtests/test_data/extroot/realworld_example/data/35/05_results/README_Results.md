# Results

All the results that are final versions of your data analysis or processing,
should be copied into this `05_results` folder. Organize your results folder in
the way most fitting to your project.

Provide metadata to your results files.
