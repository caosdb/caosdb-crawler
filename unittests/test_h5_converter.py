#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2023 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
from functools import partial
from pathlib import Path

import linkahead as db
import numpy as np
from pytest import fixture, importorskip
from utils import dircheckstr as dircheck_base

from caoscrawler.converters.hdf5_converter import (
    H5DatasetElement, H5GroupElement, H5NdarrayElement,
    convert_basic_element_with_nd_array, convert_h5_element)
from caoscrawler.debug_tree import DebugTree
from caoscrawler.scanner import scan_directory
from caoscrawler.structure_elements import (FloatElement, ListElement,
                                            TextElement)

# Skip the whole module if h5py hasn't been installed
h5py = importorskip("h5py")


UNITTESTDIR = Path(__file__).parent

# always add the path here
dircheckstr = partial(dircheck_base, UNITTESTDIR)


@fixture
def h5_dummy_file():

    path = UNITTESTDIR / "hdf5_dummy_file.hdf5"

    return h5py.File(path, 'r')


def test_h5_elements(h5_dummy_file):

    elt = convert_h5_element(h5_dummy_file["group_level1_a"], "test")
    assert isinstance(elt, H5GroupElement)

    elt = convert_h5_element(h5_dummy_file["root_integers"], "test")
    assert isinstance(elt, H5DatasetElement)


def test_nd_array_conversion():

    # Only test array handling here, `convert_basic_element` is tested
    # elsewhere.
    arr = np.array([[["something"]]])
    elt = convert_basic_element_with_nd_array(arr)
    assert isinstance(elt, TextElement)
    assert elt.value == "something"

    arr = np.zeros((1, 1))
    elt = convert_basic_element_with_nd_array(arr)
    assert isinstance(elt, FloatElement)
    assert elt.value == 0

    arr = np.zeros((1, 3, 1))
    elt = convert_basic_element_with_nd_array(arr)
    assert isinstance(elt, ListElement)
    assert elt.value == [0, 0, 0]

    arr = np.array([[1, 2, 3], [4, 5, 6]])
    elt = convert_basic_element_with_nd_array(arr, internal_path="some/path")
    assert isinstance(elt, H5NdarrayElement)
    assert elt.internal_path == "some/path"

    # Non-arrays should be forwarded correctly
    elt = convert_basic_element_with_nd_array("something")
    assert isinstance(elt, TextElement)
    assert elt.value == "something"

    elt = convert_basic_element_with_nd_array([0, 0, 0])
    assert isinstance(elt, ListElement)
    assert elt.value == [0, 0, 0]


def test_record_creation():

    dbt = DebugTree()
    records = scan_directory(UNITTESTDIR, UNITTESTDIR / "h5_cfood.yml", debug_tree=dbt)

    # In total 3 records: The file, the Dataset, and its ndarray
    assert len(records) == 3
    file_rec = [rec for rec in records if isinstance(rec, db.File)]
    # exactly on file
    assert len(file_rec) == 1

    subd = dbt.debug_tree[dircheckstr("hdf5_dummy_file.hdf5")]
    # At this level, we have 5 variables (directories and paths, plus H5File
    # record), and one record.
    assert len(subd[0]) == 5
    assert len(subd[1]) == 1
    file_rec = subd[1]["H5File"]
    assert file_rec.get_property("H5Dataset") is not None
    assert file_rec.get_property("H5Dataset").value is not None
    # Reference properties currently need to be integration tested (especially
    # with the circular dependency between) H5File and NDArray.

    # top level integers
    subd = dbt.debug_tree["root_integers"]
    # Two additional variables (RootIntegerElement + Dataset record), one
    # additional record
    assert len(subd[0]) == 7
    assert len(subd[1]) == 2
    ds_rec = subd[1]["H5Dataset"]
    assert isinstance(ds_rec, db.Record)
    assert len(ds_rec.parents) == 1
    assert ds_rec.parents[0].name == "H5Dataset"
    assert ds_rec.get_property("Ndarray") is not None
    assert ds_rec.get_property("Ndarray").value is not None
    assert ds_rec.get_property("attr_data_root") is not None
    assert isinstance(ds_rec.get_property("attr_data_root").value, list)
    for number in [-2.,  -4.,  -8., -10.12345]:
        assert number in [float(val) for val in ds_rec.get_property("attr_data_root").value]
