#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import importlib

from pathlib import Path
from pytest import fixture, mark

from caoscrawler.converters import (CrawlerTemplate, replace_variables, TextElementConverter)
from caoscrawler.crawl import Crawler
from caoscrawler.scanner import (create_converter_registry, scan_directory,
                                 scan_structure_elements)
from caoscrawler.stores import GeneralStore
from caoscrawler.structure_elements import DictElement, TextElement


UNITTESTDIR = Path(__file__).parent


@fixture
def converter_registry():
    converter_registry: dict[str, dict[str, str]] = {
        "TextElement": {
            "converter": "TextElementConverter",
            "package": "caoscrawler.converters"},
        "Directory": {
            "converter": "DirectoryConverter",
            "package": "caoscrawler.converters"},
        "CSVTableConverter": {
            "converter": "CSVTableConverter",
            "package": "caoscrawler.converters"},
        "Datetime": {
            "converter": "DatetimeElementConverter",
            "package": "caoscrawler.converters"
        }
    }

    for key, value in converter_registry.items():
        module = importlib.import_module(value["package"])
        value["class"] = getattr(module, value["converter"])
    return converter_registry


def test_issue_10():
    """Test integer-to-float conversion in dictionaries"""
    crawler_definition = {
        "DictTest": {
            "type": "DictElement",
            "match": "(.*)",
            "records": {
                "TestRec": {}
            },
            "subtree": {
                "float_element": {
                    "type": "DictFloatElement",
                    "match_name": "float_value",
                    "match_value": "(?P<float_value>.*)",
                    "records": {
                        "TestRec": {
                            "float_prop": "$float_value"
                        }
                    }
                }
            }
        }
    }

    converter_registry = create_converter_registry(crawler_definition)

    test_dict = {
        "float_value": 4
    }

    records = scan_structure_elements(
        DictElement("TestDict", test_dict), crawler_definition, converter_registry)
    assert len(records) == 1
    assert records[0].parents[0].name == "TestRec"
    assert records[0].get_property("float_prop") is not None
    assert float(records[0].get_property("float_prop").value) == 4.0


@mark.xfail(reason="FIX: https://gitlab.com/caosdb/caosdb-crawler/-/issues/47")
def test_list_datatypes():
    crawler_definition = {
        "DictTest": {
            "type": "DictElement",
            "match": "(.*)",
            "records": {
                "Dataset": {}
            },
            "subtree": {
                "int_element": {
                    "type": "IntegerElement",
                    "match_name": ".*",
                    "match_value": "(?P<int_value>.*)",
                    "records": {
                        "Dataset": {
                            "Subject": "+$int_value"
                        }
                    }
                }
            }
        }
    }

    crawler = Crawler()
    converter_registry = crawler.load_converters(crawler_definition)

    test_dict = {
        "v1": 1233,
        "v2": 1234
    }

    records = crawler.start_crawling(
        DictElement("TestDict", test_dict), crawler_definition, converter_registry)
    assert len(records) == 1
    assert records[0].parents[0].name == "Dataset"
    assert records[0].get_property("Subject") is not None
    assert isinstance(records[0].get_property("Subject").value, list)
    assert records[0].get_property("Subject").datatype is not None
    assert records[0].get_property("Subject").datatype.startswith("LIST")


def test_issue_93():
    """https://gitlab.com/linkahead/linkahead-crawler/-/issues/93

    cfood.yaml does not allow umlaut in $expression"""
    values = GeneralStore()
    expressions = [
        "foo",
        "foo.bär",
        "_1",
        "Ä",
        "ųøîµ",
    ]
    for exp in expressions:
        values[exp] = f"This is {exp}"
    # ## Test preliminary check
    # With braces
    for exp in expressions:
        assert replace_variables(f"${{{exp}}}", values) == f"This is {exp}"
    # Without braces
    for exp in expressions:
        assert replace_variables(f"${exp}", values) == f"This is {exp}"

    # ## Test actual replacement
    for exp in expressions:
        # as-is
        propvalue = f"${{{exp}}}"
        propvalue_template = CrawlerTemplate(propvalue)
        # from IPython import embed
        # embed()

        assert propvalue_template.safe_substitute(**values.get_storage()) == f"This is {exp}"

        # String embedded into context
        propvalue = f"some text before >> ${{{exp}}} << some text after"
        print(propvalue)
        propvalue_template = CrawlerTemplate(propvalue)
        assert (propvalue_template.safe_substitute(**values.get_storage())
                == f"some text before >> This is {exp} << some text after")


def test_issue_112(converter_registry):
    """Test that empty table cells are not matched in case of
    ``match_value: ".+"``.

    See https://gitlab.com/linkahead/linkahead-crawler/-/issues/112.

    """
    tec = TextElementConverter(
        name="TestTextConverter",
        definition={
            "match_name": ".*",
            "match_value": "(?P<content>.+)"
        },
        converter_registry=converter_registry
    )

    empty = TextElement(name="empty", value='')
    assert tec.match(empty) is None

    empty_none = TextElement(name="empty", value=None)
    assert tec.match(empty_none) is None

    non_empty = TextElement(name="empty", value=' ')
    matches = tec.match(non_empty)
    assert "content" in matches
    assert matches["content"] == ' '

    # Cfood definition for CSV example file
    records = scan_directory(UNITTESTDIR / "test_directories" / "examples_tables" / "ExperimentalData",
                             UNITTESTDIR / "test_directories" / "examples_tables" / "crawler_for_issue_112.yml")
    assert records
    for rec in records:
        print(rec.name)
        assert len(rec.parents.filter_by_identity(name="Event")) > 0
        assert rec.name in ["event_a", "event_b", "event_c"]
        if rec.name == "event_a":
            assert rec.get_property("event_time") is not None
            assert rec.get_property("event_time").value == "2025-02-06"
        if rec.name == "event_b":
            # `date` field is empty, so there must be no match
            assert rec.get_property("event_time") is None
        if rec.name == "event_c":
            assert rec.get_property("event_time") is not None
            assert rec.get_property("event_time").value == "2025-02-06T09:00:00"
