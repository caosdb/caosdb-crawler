# Getting started with the CaosDB Crawler #

## Installation
see INSTALL.md

## Run Unit Tests

1. Install additional dependencies:
  - h5py
2. Run `pytest unittests`.

## Documentation ##
We use sphinx to create the documentation. Docstrings in the code should comply
with the Googly style (see link below).

Build documentation in `src/doc` with `make doc`. Note that for the
automatic generation of the complete API documentation, it is
necessary to first install this library with all its optional
dependencies, i.e., `pip install .[h5-crawler,spss]`.

### Requirements ###

- `sphinx`
- `sphinx-autoapi`
- `recommonmark`
- `sphinx-rtd-theme`

### How to contribute ###

- [Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
- [Google Style Python Docstrings 2nd reference](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings)
- [References to other documentation](https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#role-external)


