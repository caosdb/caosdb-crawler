#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
test the JSON converter
"""
import os
from pathlib import Path

import linkahead as db
from pytest import raises

from caoscrawler.converters import JSONFileConverter
from caoscrawler.crawl import Crawler
from caoscrawler.scanner import (create_converter_registry, load_definition,
                                 scan_structure_elements)
from caoscrawler.structure_elements import File, JSONFile

UNITTESTDIR = Path(__file__).parent


def test_json():
    crawler_definition_path = (UNITTESTDIR / "test_directories" / "examples_json"
                               / "jsontest_cfood.yml")
    json_file_path = UNITTESTDIR / "test_directories" / "examples_json" / "testjson.json"

    crawler_definition = load_definition(crawler_definition_path)
    # Load and register converter packages:
    converter_registry = create_converter_registry(crawler_definition)

    records = scan_structure_elements(
        JSONFile(os.path.basename(json_file_path), json_file_path),
        crawler_definition,
        converter_registry
    )

    rec = [r for r in records if r.name == "DEMO"]
    assert len(rec) == 1
    rec = rec[0]
    assert len(rec.parents) == 1
    assert rec.parents[0].name == "Project"
    assert rec.get_property("url") is not None
    assert rec.get_property("url").value == "https://site.de/index.php/"
    assert rec.get_property("Person") is not None
    assert isinstance(rec.get_property("Person").value, list)
    assert len(rec.get_property("Person").value) == 2


def test_broken_validation():
    crawler_definition_path = UNITTESTDIR / "broken_cfoods" / "broken_validation_path.yml"
    with raises(FileNotFoundError) as err:
        crawler_definition = load_definition(crawler_definition_path)

    assert str(err.value).startswith("Couldn't find validation file")
