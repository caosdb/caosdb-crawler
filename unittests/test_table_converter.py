#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Alexander Schlemmer <alexander.schlemmer@ds.mpg.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
test the converters module
"""

import importlib
import math
from os.path import basename, dirname, join
from pathlib import Path

import linkahead as db
import pytest
from utils import dircheckstr

from caoscrawler import Crawler
from caoscrawler.converters import (Converter, ConverterValidationError,
                                    CSVTableConverter, DictConverter,
                                    XLSXTableConverter)
from caoscrawler.debug_tree import DebugTree
from caoscrawler.identifiable_adapters import (IdentifiableAdapter,
                                               LocalStorageIdentifiableAdapter)
from caoscrawler.scanner import scan_directory
from caoscrawler.stores import GeneralStore
from caoscrawler.structure_elements import (BooleanElement, DictElement,
                                            Directory, File, FloatElement,
                                            IntegerElement, ListElement,
                                            TextElement)

UNITTESTDIR = Path(__file__).parent


@pytest.fixture
def converter_registry():
    converter_registry: dict[str, dict[str, str]] = {
        "Directory": {
            "converter": "DirectoryConverter",
            "package": "caoscrawler.converters"},
        "CSVTableConverter": {
            "converter": "CSVTableConverter",
            "package": "caoscrawler.converters"},
        "XLSXTableConverter": {
            "converter": "XLSXTableConverter",
            "package": "caoscrawler.converters"},
        "DictElement": {
            "converter": "DictElementConverter",
            "package": "caoscrawler.converters"},
        "TextElement": {
            "converter": "TextElementConverter",
            "package": "caoscrawler.converters"},
        "IntegerElement": {
            "converter": "IntegerElementConverter",
            "package": "caoscrawler.converters"},
        "FloatElement": {
            "converter": "FloatElementConverter",
            "package": "caoscrawler.converters"},
    }


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return join(dirname(__file__), *pathcomponents)


def test_convert_table(converter_registry):
    extentions = ["xlsx", "csv", "tsv"]
    if importlib.util.find_spec("odf") is not None:
        extentions.append("ods")
    for file_ext in extentions:
        def_opt = {"skiprows": ["1", "2"], "header": 0}
        if file_ext == "tsv":
            def_opt["sep"] = "\t"
        if file_ext in ["csv", "tsv"]:
            converter = CSVTableConverter(
                def_opt,
                "Tab",
                converter_registry)
        else:
            converter = XLSXTableConverter(
                def_opt,
                "Tab",
                converter_registry)
        store = GeneralStore()
        file_element = File("table." + file_ext,
                            rfp("test_tables", "test1." + file_ext))
        res = converter.create_children(store,
                                        file_element)
        assert len(res) == 5
        for i in range(5):
            assert res[i].name == str(i)
            assert type(res[i].name) == str
            assert type(res[i].value) == dict
            assert len(res[i].value) == 4
            assert type(res[i].value["Col_1"]) == int
            assert res[i].value["Col_1"] == i
            assert type(res[i].value["Col_2"]) == float
            assert type(res[i].value["Col_3"]) == int
            if i != 3:
                assert type(res[i].value["text"]) == str
            else:
                assert type(res[i].value["text"]) == float  # the nan value
                assert math.isnan(res[i].value["text"])

    # Using an index col:
    converter = XLSXTableConverter(
        {"skiprows": ["1", "2"], "header": 0, "index_col": "3"},
        "XLSXTable",
        converter_registry)
    store = GeneralStore()
    file_element = File("table.xlsx",
                        rfp("test_tables", "test1.xlsx"))
    res = converter.create_children(store,
                                    file_element)
    assert res[0].name == "jdsfkljadskf"


def test_crawl_csv_table():
    dbt = DebugTree()
    scan_directory(rfp("test_directories", "examples_tables", "ExperimentalData"),
                   rfp("test_directories", "examples_tables", "crawler_for_tables.yml"),
                   debug_tree=dbt)
    for file_ext in ["xlsx", "csv"]:
        subd = dbt.debug_tree[dircheckstr(
            UNITTESTDIR / "test_directories" / "examples_tables" / "ExperimentalData",
            "test1." + file_ext)]
        record_experiment = subd[1]["Experiment"]
        assert isinstance(record_experiment, db.Record)
        assert isinstance(record_experiment.get_property("Measurements").value, list)
        assert len(record_experiment.get_property("Measurements").value) == 5
        prop_measure = record_experiment.get_property("Measurements").value[2]
        assert isinstance(prop_measure, db.Record)
        assert prop_measure.get_property("Col_1").value == "2"
