#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
from unittest.mock import MagicMock, Mock, patch

import linkahead as db
import pytest
from test_crawler import basic_retrieve_by_name_mock_up, mock_get_entity_by

from caoscrawler.exceptions import ImpossibleMergeError
from caoscrawler.identifiable import Identifiable
from caoscrawler.identifiable_adapters import CaosDBIdentifiableAdapter
from caoscrawler.sync_graph import SyncGraph
from caoscrawler.sync_node import SyncNode, parent_in_list, property_in_list


def assert_parents_equal(p1, p2):
    """Special assertion for comparing parents."""
    for a, b in zip(p1, p2):
        assert a.id == b.id
        assert a.name == b.name


def assert_properties_equal(p1, p2):
    """Special assertion for comparing properties."""
    for a, b in zip(p1, p2):
        assert a.id == b.id
        assert a.name == b.name
        assert a.value == b.value
        assert a.datatype == b.datatype


def test_sync_node():
    # initialization
    rec = (db.Record(id=101, name='101')
           .add_parent("A")
           .add_parent("B")
           .add_parent(id=102)
           .add_property(name="a", value='a')
           .add_property(id=103, value='b'))
    rec.description = "hallo"
    sna = SyncNode(rec)
    # check information stored in initialized SyncNode
    assert "Record" in str(sna)
    assert sna.id == rec.id
    assert sna.role == rec.role
    assert sna.name == rec.name
    assert sna.description == rec.description
    assert_parents_equal(sna.parents, rec.parents)
    assert_properties_equal(sna.properties, rec.properties)
    # ... special case File (path and file attributes)
    fi = db.File(id=101, name='101', path='/a/')
    snb = SyncNode(fi)
    assert snb.role == fi.role
    assert snb.name == fi.name
    assert snb.id == fi.id
    assert snb.path == fi.path
    assert snb.file == fi.file

    # check information in exported db.Entity
    export = sna.export_entity()
    assert export.id == rec.id
    assert export.role == rec.role
    assert export.name == rec.name
    assert export.description == rec.description
    assert_parents_equal(export.parents, rec.parents)
    assert_properties_equal(export.properties, rec.properties)
    export = snb.export_entity()
    assert export.role == fi.role
    assert export.name == fi.name
    assert export.id == fi.id
    assert export.path == fi.path
    assert export.file == fi.file

    # merge no common information
    # ---------------------------
    rec_a = (db.Record(name='101')
             .add_parent("A")
             .add_parent(id=102)
             .add_property(name="a", value='a')
             .add_property(id=103, value='b'))

    rec_b = (db.Record(id=101)
             .add_parent("B")
             .add_parent(id=103)
             .add_property(name="a", value='a')
             .add_property(id=103, value='b'))
    rec_b.description = "tja"

    sn_a = SyncNode(rec_a)
    sn_b = SyncNode(rec_b)
    sn_a.update(sn_b)
    # test information in updated node
    assert sn_a.id == rec_b.id
    assert sn_a.role == rec_a.role
    assert sn_a.name == rec_a.name
    assert sn_a.description == rec_b.description
    for p in rec_a.parents + rec_b.parents:
        assert p in sn_a.parents
    for p in rec_a.properties + rec_b.properties:
        assert p in sn_a.properties
    # Check for duplicated property:
    ps = [p for p in sn_a.properties if p.name == "a"]
    assert len(ps) == 2
    assert ps[0].value == "a"
    assert ps[1].value == "a"

    # test information in exported entity
    export = sn_a.export_entity()
    assert export.id == rec_b.id
    assert export.name == rec_a.name
    for p in rec_a.parents + rec_b.parents:
        assert parent_in_list(p, export.parents)
    for p in rec_a.properties + rec_b.properties:
        if p.name is not None:
            assert p.name in [el.name for el in export.properties]
        if p.id is not None:
            assert p.id in [el.id for el in export.properties]
    assert len(export.properties) == 2
    assert export.get_property('a').value == 'a'
    assert export.get_property(103).value == 'b'
    assert export.description == rec_b.description
    assert export.role == rec_a.role

    # merge with common information
    # -----------------------------
    rec_a = (db.Record(id=101, name='101')
             .add_parent("A")
             .add_parent(id=102)
             .add_property(name="a", value='a'))

    rec_b = (db.Record(id=101, name='101')
             .add_parent("A")
             .add_parent(id=102)
             .add_property(name="a", value='a'))

    sn_a = SyncNode(rec_a)
    sn_b = SyncNode(rec_b)
    sn_a.update(sn_b)
    assert sn_a.id == rec_b.id
    assert sn_a.name == rec_a.name
    for p in rec_a.parents + rec_b.parents:
        assert parent_in_list(p, sn_a.parents)
    for p in rec_a.properties + rec_b.properties:
        assert property_in_list(p, sn_a.properties)
    assert sn_a.description == rec_b.description
    assert sn_a.role == rec_a.role

    # merge with conflicting information
    # ----------------------------------
    # ID mismatch
    sn_a = SyncNode(db.Record(id=102))
    with pytest.raises(ImpossibleMergeError, match="Trying to update"):
        sn_a.update(SyncNode(db.Record(id=101)))

    # name mismatch
    sn_a = SyncNode(db.Record(name='102'))
    with pytest.raises(ImpossibleMergeError, match="Trying to update"):
        sn_a.update(SyncNode(db.Record(name='101')))

    # type mismatch
    sn_a = SyncNode(db.Record(name='102'))
    with pytest.raises(ImpossibleMergeError, match="Trying to update"):
        sn_a.update(SyncNode(db.File(name='102')))

    # description mismatch
    sn_a = SyncNode(db.Record(description='102'))
    with pytest.raises(ImpossibleMergeError, match="Trying to update"):
        sn_a.update(SyncNode(db.Record(description='101')))

    # path mismatch
    sn_a = SyncNode(db.File(path='102'))
    with pytest.raises(ImpossibleMergeError, match="Trying to update"):
        sn_a.update(SyncNode(db.File(path='101')))

    # identifiable mismatch
    sn_a = SyncNode(db.File(path='102'))
    sn_a.identifiable = Identifiable(name='a')
    sn_b = SyncNode(db.File(path='101'))
    sn_b.identifiable = Identifiable(name='b')
    with pytest.raises(ValueError, match="identifiable"):
        sn_a.update(sn_b)


def test_export_node():
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_parent(id=103)
             .add_property(name="a", value=[SyncNode(db.Record())])
             .add_property(name='b', id=103, value='b'))

    sn_a = SyncNode(rec_a)
    exp = sn_a.export_entity()
    assert exp.id == rec_a.id
    assert exp.name == rec_a.name
    for p in rec_a.parents:
        assert len([el for el in exp.parents if p.name == el.name]) == 1
    for p in rec_a.properties:
        assert p.value == exp.get_property(p.name).value
        if isinstance(p.value, list):
            assert len(p.value) == len(exp.get_property(p.name).value)
    assert len(exp.properties) == len(rec_a.properties)
    assert len(exp.parents) == len(rec_a.parents)

    # ---------------------------------------------------------------------------------------------
    # NOTE: in the following we create a SyncNode object with twice the same Property as a short
    # hand for a SyncNode that was created from one Entity with such a Property and then updating
    # it with another SyncNode that also has the Property
    # ---------------------------------------------------------------------------------------------

    # same property name, different values
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value='b')
             .add_property(name="a", value='a'))

    # there should be a warning when multiproperties are used
    with pytest.warns(UserWarning) as caught:
        SyncNode(rec_a)
        messages = {str(w.message) for w in caught}
        assert ("Multiproperties are not supported by the crawler.") in messages

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()
    assert "The problematic property is 'a' with values '['b']' and '['a']'" in str(ime.value)

    # SyncNodes with same ID are considered equal
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=SyncNode(db.Record(id=1)))
             .add_property(name="a", value=SyncNode(db.Record(id=1))))

    exp = SyncNode(rec_a).export_entity()
    assert exp.get_property('a').value.id == 1
    # SyncNodes convert multi properties into single properties
    assert len([p for p in exp.properties if p.name == "a"]) == 1

    # same SyncNode object is obviously equal
    sn = SyncNode(db.Record(id=1))
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=sn)
             .add_property(name="a", value=sn))

    exp = SyncNode(rec_a).export_entity()
    assert exp.get_property('a').value.id == 1
    assert len([p for p in exp.properties if p.name == "a"]) == 1

    # different SyncNode Objects (without an ID) are not equal
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=SyncNode(db.Record()))
             .add_property(name="a", value=SyncNode(db.Record())))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = (f"The problematic property is 'a' with values '[{SyncNode(db.Record())}]' "
           f"and '[{SyncNode(db.Record())}]'")
    assert msg in str(ime.value)

    # different SyncNode Objects with differing ID are not equal
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=SyncNode(db.Record(id=1)))
             .add_property(name="a", value=SyncNode(db.Record(id=2))))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = (f"The problematic property is 'a' with values '[{SyncNode(db.Record(id=1))}]' "
           f"and '[{SyncNode(db.Record(id=2))}]'")
    assert msg in str(ime.value)

    # SyncNodes with same ID are considered equal (list)
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=2))])
             .add_property(name="a", value=[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=2))]))

    exp = SyncNode(rec_a).export_entity()
    assert exp.get_property('a').value[0].id == 1
    assert len([p for p in exp.properties if p.name == "a"]) == 1

    # SyncNodes with same ID are not equal when in different order (list)
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=2))])
             .add_property(name="a", value=[SyncNode(db.Record(id=2)), SyncNode(db.Record(id=1))]))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = ("The problematic property is 'a' with values "
           f"'{[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=2))]}' "
           f"and '{[SyncNode(db.Record(id=2)), SyncNode(db.Record(id=1))]}'")
    assert msg in str(ime.value)

    # same SyncNode object is obviously equal (list)
    sn = SyncNode(db.Record(id=1))
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[sn])
             .add_property(name="a", value=[sn]))

    exp = SyncNode(rec_a).export_entity()
    assert exp.get_property('a').value[0].id == 1

    # different SyncNode Objects are not equal (list)
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[SyncNode(db.Record())])
             .add_property(name="a", value=[SyncNode(db.Record())]))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = ("The problematic property is 'a' with values "
           f"'{[SyncNode(db.Record())]}' and '{[SyncNode(db.Record())]}'")
    assert msg in str(ime.value)

    # different SyncNode Objects with differing are not equal (list)
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[SyncNode(db.Record(id=1))])
             .add_property(name="a", value=[SyncNode(db.Record(id=2))]))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = ("The problematic property is 'a' with values "
           f"'{[SyncNode(db.Record(id=1))]}' and '{[SyncNode(db.Record(id=2))]}'")
    assert msg in str(ime.value)

    # list vs no list
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=SyncNode(db.Record(id=1)))
             .add_property(name="a", value=[SyncNode(db.Record(id=1))]))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()
    msg = ("The problematic property is 'a' with values "
           f"'[{SyncNode(db.Record(id=1))}]' and '{[SyncNode(db.Record(id=1))]}'")
    assert msg in str(ime.value)

    # different list sizes
    rec_a = (db.Record(id=101)
             .add_parent("B")
             .add_property(name="a", value=[SyncNode(db.Record(id=1))])
             .add_property(name="a", value=[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=1))]))

    with pytest.raises(ImpossibleMergeError) as ime:
        exp = SyncNode(rec_a).export_entity()

    msg = ("The problematic property is 'a' with values "
           f"'{[SyncNode(db.Record(id=1))]}' and "
           f"'{[SyncNode(db.Record(id=1)), SyncNode(db.Record(id=1))]}'")
    assert msg in str(ime.value)
